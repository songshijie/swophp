<?php
/**
 * Author: top-songshijie
 * DateTime: 2020/03/21 12:53
 */

use SwoPhp\Console\Input;
use SwoPhp\Foundation\Application;

if (!function_exists('app')) {
    function app($abstract = null)
    {
        if (empty($abstract)) {
            return Application::getInstance();
        }
        return Application::getInstance()->make($abstract);
    }
}

if (!function_exists('dd')) {
    function dd($message, $description = null)
    {
        Input::info($message, $description);
    }
}
