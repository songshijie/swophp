<?php
/**
 * Author: top-songshijie
 * DateTime: 2020/03/22 16:45
 */

namespace SwoPhp\Config;

class Config
{
    protected $items = null;

    protected $configPath = null;

    public function __construct()
    {
        $this->configPath = app()->getBasePath().'/config';

        $this->items = $this->phpParser();
    }

    protected function phpParser()
    {
        $files = scandir($this->configPath);
        $data = null;
        foreach ($files as $key => $file) {
            if ($file === '.' || $file === '..') {
                continue;
            }
            $filename = stristr($file, ".php", true);
            $data[$filename] = include $this->configPath."/".$file;
        }
        return $data;
    }

    public function get($keys)
    {
        $data = $this->items;
        foreach (explode('.', $keys) as $key){
            $data = $data[$key];
        }
        return $data;
    }

}
