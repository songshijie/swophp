<?php
/*
 * Author: top-songshijie
 * DateTime: 2020/3/25 14:11
 *
                   _ooOoo_
                  o8888888o
                  88" . "88
                  (| -_- |)
                  O\  =  /O
               ____/`---'\____
             .'  \\|     |//  `.
            /  \\|||  :  |||//  \
           /  _||||| -:- |||||-  \
           |   | \\\  -  /// |   |
           | \_|  ''\---/''  |   |
           \  .-\__  `-`  ___/-. /
         ___`. .'  /--.--\  `. . __
      ."" '<  `.___\_<|>_/___.'  >'"".
     | | :  `- \`.;`\ _ /`;.`/ - ` : | |
     \  \ `-.   \_ __\ /__ _/   .-` /  /
======`-.____`-.___\_____/___.-`____.-'======
                   `=---='
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
         佛祖保佑       永无BUG
*
*/

namespace SwoPhp\Server\WebSocket;


class Connections
{
    protected static $connections = [];

    public static function init($fd,$path)
    {
        self::$connections[$fd]['path'] = $path;
    }

    public static function get($fd)
    {
        return self::$connections[$fd];
    }

    public static function destory($fd)
    {
        unset(self::$connections[$fd]);
    }

}