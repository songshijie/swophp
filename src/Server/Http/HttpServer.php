<?php
/**
 * Author: top-songshijie
 * DateTime: 2020/03/20 23:54
 */

namespace SwoPhp\Server\Http;

use SwoPhp\Server\Server;

class HttpServer extends Server
{
    public function initSetting()
    {
        $config = app('config');
        $this->port = $config->get('server.http.port');
        $this->host = $config->get('server.http.host');
        $this->config = $config->get('server.http.swoole');
    }

    public function createServer()
    {
        $this->swooleServer = new \Swoole\Http\Server($this->host,$this->port);
        echo "
                               SERVER INFORMATION
********************************************************************************
* HTTP     | Listen: {$this->host}:{$this->port}
********************************************************************************
        ";
    }

    public function initEvent()
    {
        $this->setEvent('sub',['request'=>'onRequest']);
    }

    public function onRequest($request, $response)
    {
        $uri = $request->server['request_uri'];
        if ($uri == '/favicon.ico') {
            $response->status(404);
            $response->end('');
            return null;
        }

        $return =app('route')->setFlag('Http')->setMethod($request->server['request_method'])->match($request->server['request_uri']);
        $response->end($return);
    }
}
