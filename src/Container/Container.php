<?php
/**
 * Author: top-songshijie
 * DateTime: 2020/03/21 10:43
 */

namespace SwoPhp\Container;

use Exception;

class Container
{
    protected static $instance = null;

    protected $bindings = [];

    public static function setInstance($container = null)
    {
        return static::$instance = $container;
    }

    public static function getInstance()
    {
        if (is_null(static::$instance)) {
            static::$instance = new static();
        }
        return static::$instance;
    }

    public function bind($abstract, $object)
    {
        $this->bindings[$abstract] = $object;
    }

    public function make($abstract)
    {
        if (!isset($this->bindings[$abstract])){
            throw new Exception('容器'.$abstract.'不存在', 500);
        }
        return $this->bindings[$abstract];
    }

}
