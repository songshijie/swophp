<?php
/**
 * Author: top-songshijie
 * DateTime: 2020/03/21 00:15
 */

namespace SwoPhp\Foundation;


use SwoPhp\Route\Route;
use SwoPhp\Container\Container;
use SwoPhp\Server\Http\HttpServer;
use SwoPhp\Server\WebSocket\WebSocketServer;

class Application extends Container
{
    const SWOPHP_WELCOME = "
      _____                 _____   _            
     / ____|               |  __ \ | |           
    | (___ __      __ ___  | |__) || |__   _ __  
     \___ \\ \ /\ / // _ \ |  ___/ | '_ \ | '_ \ 
     ____) |\ V  V /| (_) || |     | | | || |_) |
    |_____/  \_/\_/  \___/ |_|     |_| |_|| .__/ 
                                          | |    
                                          |_|";

    protected $basePath = null;

    public function __construct($basePath = null)
    {
        echo self::SWOPHP_WELCOME . "\n";
        $this->setBasePath($basePath);
        $this->init();
    }

    public function init()
    {
        self::setInstance($this);
        $binds = [
            'route'  => Route::getInstance()->registerRoute(),
            'config' => new \SwoPhp\Config\Config(),
        ];
        foreach ($binds as $abstract => $object) {
            $this->bind($abstract, $object);
        }
    }

    public function run($argv)
    {
        switch ($argv[1]) {
            case  'http:start':
                $server = new HttpServer();
                break;
            case 'ws:start':
                $server = new WebSocketServer();
                break;
            default:
                $server = null;
        }
        if (is_null($server)){
            echo "参数有误!\n";return;
        }
        $server->start();
    }

    public function setBasePath($basePath)
    {
        $this->basePath = rtrim($basePath, '\/');
    }

    public function getBasePath()
    {
        return $this->basePath;
    }

}
